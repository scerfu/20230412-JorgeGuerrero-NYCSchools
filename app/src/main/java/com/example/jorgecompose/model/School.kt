package com.example.jorgecompose.model

import com.google.gson.annotations.SerializedName

/**
 * Data class to hold School information
 */
data class School(
    @SerializedName("city")
    val city: String? = null,
    @SerializedName("dbn")
    val dbn: String? = null,
    @SerializedName("language_classes")
    val languageClasses: String? = null,
    @SerializedName("latitude")
    val latitude: String? = null,
    @SerializedName("location")
    val location: String? = null,
    @SerializedName("longitude")
    val longitude: String? = null,
    @SerializedName("overview_paragraph")
    val overviewParagraph: String? = null,
    @SerializedName("phone_number")
    val phoneNumber: String? = null,
    @SerializedName("primary_address_line_1")
    val primaryAddressLine1: String? = null,
    @SerializedName("school_email")
    val schoolEmail: String? = null,
    @SerializedName("school_name")
    val schoolName: String? = null,
    @SerializedName("state_code")
    val stateCode: String? = null,
    @SerializedName("subway")
    val subway: String? = null,
    @SerializedName("total_students")
    val totalStudents: String? = null,
    @SerializedName("transfer")
    val transfer: String? = null,
    @SerializedName("website")
    val website: String? = null,
    @SerializedName("zip")
    val zip: String? = null
)
