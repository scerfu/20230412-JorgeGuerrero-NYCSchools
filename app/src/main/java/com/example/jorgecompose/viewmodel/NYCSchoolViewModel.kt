package com.example.jorgecompose.viewmodel

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.jorgecompose.model.Sat
import com.example.jorgecompose.model.School
import com.example.jorgecompose.service.NYCSchoolRepository
import com.example.jorgecompose.utils.UIState
import kotlinx.coroutines.launch

class NYCSchoolViewModel(
    private val repository: NYCSchoolRepository
) : ViewModel() {
    init {
        getNYCSchools()
    }

    var selectedSchool: School? = null
    var selectedSat: Sat? = null

    private val _schools: MutableState<UIState<List<School>>> =
        mutableStateOf(UIState.LOADING)
    val schools: State<UIState<List<School>>> get() = _schools

    private val _sat: MutableState<UIState<Sat?>> =
        mutableStateOf(UIState.LOADING)
    val sat: State<UIState<Sat?>> get() = _sat

    private fun getNYCSchools() {
        viewModelScope.launch {
            repository.getNYCSchools().collect() {
                _schools.value = it
            }
        }
    }

    fun getSatScore() {
        selectedSchool?.dbn?.let { dbn ->
            viewModelScope.launch {
                repository.getSatScore(dbn).collect() {
                    _sat.value = it
                }
            }
        }

    }
}