package com.example.jorgecompose.utils

/**
 * States that we will be emiting for the state flow
 */
sealed class UIState<out T>{
    object LOADING : UIState<Nothing>()
    data class SUCCESS<T> (val response: T): UIState<T>()
    data class ERROR(val error: Exception) : UIState<Nothing>()
}
