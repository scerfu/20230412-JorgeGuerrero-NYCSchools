package com.example.jorgecompose.utils

/**
 * Defining the constants for the network call
 */
const val BASE_URL = "https://data.cityofnewyork.us/resource/"
const val ENDPOINT_SCHOOLS = "s3k6-pzi2.json"
const val ENDPOINT_SAT_SCORES = "f9bf-2cp4.json"