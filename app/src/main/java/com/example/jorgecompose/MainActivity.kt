package com.example.jorgecompose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.jorgecompose.ui.theme.JorgeComposeTheme
import com.example.jorgecompose.utils.UIState
import com.example.jorgecompose.view.SatList
import com.example.jorgecompose.view.SchoolsList
import com.example.jorgecompose.viewmodel.NYCSchoolViewModel
import org.koin.androidx.compose.koinViewModel

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /**
         * Defining navigation for the app.
         * Assigning the ViewModel with the use of koin
         */
        setContent {
            JorgeComposeTheme() {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier
                        .fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    val navController = rememberNavController()
                    val schoolsViewModel = koinViewModel<NYCSchoolViewModel>()
                    NavHost(navController = navController, startDestination = "main") {
                        composable(route = "main") {
                            MainScreen(schoolsViewModel, navController = navController)
                        }
                        composable(route = "details") {
                            // I'll use a local storage like room to avoid network calls
                            schoolsViewModel.getSatScore()
                            SatScreen(schoolsViewModel, navController = navController)
                        }
                    }
                }
            }
        }
    }
}

/**
 * Evaluating UIState to act accordingly
 */
@Composable
fun MainScreen(
    schoolsViewModel: NYCSchoolViewModel,
    navController: NavController
) {
    when(val state = schoolsViewModel.schools.value) {
        is UIState.LOADING -> {}
        is UIState.SUCCESS -> {
            SchoolsList(state.response, navController) {
                schoolsViewModel.selectedSchool = it
            }
        }
        /**
         * Need to implement proper error handling like creating custom Error class
         */
        is UIState.ERROR -> {}
    }
}

/**
 * Evaluating UIState to act accordingly
 */
@Composable
fun SatScreen(
    schoolsViewModel: NYCSchoolViewModel,
    navController: NavController
) {
    when(val state = schoolsViewModel.sat.value) {
        is UIState.LOADING -> {}
        is UIState.SUCCESS -> {
            SatList(state.response, navController){
             schoolsViewModel.selectedSat = it
            }
        }
        /**
         * Need to implement proper error handling like creating custom Error class
         */
        is UIState.ERROR -> {}
    }
}