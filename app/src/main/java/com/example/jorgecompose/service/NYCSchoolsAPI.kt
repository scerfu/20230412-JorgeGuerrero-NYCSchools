package com.example.jorgecompose.service

import com.example.jorgecompose.model.Sat
import com.example.jorgecompose.model.School
import com.example.jorgecompose.utils.ENDPOINT_SAT_SCORES
import com.example.jorgecompose.utils.ENDPOINT_SCHOOLS
import retrofit2.Response
import retrofit2.http.GET

/**
 * Defining the type of response for each method
 */
interface NYCSchoolsAPI {

    @GET(ENDPOINT_SCHOOLS)
    suspend fun getNYCSchools(): Response<List<School>>

    @GET(ENDPOINT_SAT_SCORES)
    suspend fun getSatScore(): Response<List<Sat>>
}