package com.example.jorgecompose.service

import com.example.jorgecompose.model.Sat
import com.example.jorgecompose.model.School
import com.example.jorgecompose.utils.UIState
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

/**
 * Interface to expose the methods
 */
interface NYCSchoolRepository {
    fun getNYCSchools(): Flow<UIState<List<School>>>
    fun getSatScore(dbn:String): Flow<UIState<Sat?>>
}

/**
 * Implementation of the methods
 */
class NYCSchoolsRepositoryImpl(
    private val serviceApi: NYCSchoolsAPI,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : NYCSchoolRepository {

    override fun getNYCSchools(): Flow<UIState<List<School>>> = flow {
        emit(UIState.LOADING)

        try {
            val response = serviceApi.getNYCSchools()
            if (response.isSuccessful) {
                response.body()?.let {
                    emit(UIState.SUCCESS(it))
                } ?: throw Exception("Schools are not available")
            } else {
                throw Exception(response.errorBody()?.string())
            }
        } catch (e: Exception) {
            emit(UIState.ERROR(e))
        }
    }.flowOn(ioDispatcher)

    override fun getSatScore(dbn: String): Flow<UIState<Sat?>> = flow {
        emit(UIState.LOADING)

        try {
            val response = serviceApi.getSatScore()
            if (response.isSuccessful) {
                response.body()?.let {
                    emit(UIState.SUCCESS(it.firstOrNull { it.dbn == dbn }))
                } ?: throw Exception("Sat information is not available")
            } else {
                throw Exception(response.errorBody()?.string())
            }
        } catch (e: Exception) {
            emit(UIState.ERROR(e))
        }
    }.flowOn(ioDispatcher)
}
