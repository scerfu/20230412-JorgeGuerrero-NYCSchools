package com.example.jorgecompose.view

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.ExperimentalUnitApi
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.example.jorgecompose.model.School

/**
 * Present each school of the list in column
 */
@Composable
fun SchoolsList(
    schools: List<School>,
    navController: NavController,
    selectedSchool: (School) -> Unit
) {
    LazyColumn {
        itemsIndexed(items = schools) { _, school ->
            SchoolItem(school = school, navController, selectedSchool)
        }
    }
}

/**
 * Ui for the school item
 */
@OptIn(ExperimentalMaterialApi::class, ExperimentalUnitApi::class, ExperimentalFoundationApi::class)
@Composable
fun SchoolItem(
    school: School,
    navController: NavController? = null,
    selectedSchool: ((School) -> Unit)? = null
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp),
        onClick = {
            selectedSchool?.invoke(school)
            navController?.navigate("details")
        }
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = school.schoolName ?: "Invalid name",
                fontSize = TextUnit(24F, TextUnitType.Sp),
                textAlign = TextAlign.Justify,
                fontStyle = FontStyle(1)
            )
            Text(text = school.schoolEmail ?: "No email")
            Text(text = school.primaryAddressLine1 ?: "No address")
            Text(text = school.phoneNumber ?: "No phone number")
        }
    }
}