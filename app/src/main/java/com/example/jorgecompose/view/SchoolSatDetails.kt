package com.example.jorgecompose.view

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.example.jorgecompose.model.Sat

/**
 * List of Sat information
 */
@Composable
fun SatList(
    sats: Sat? = null,
    navController: NavController,
    selectedSat: (Sat) -> Unit
) {
    LazyColumn {
        itemsIndexed(items = listOf(sats)) { _, sat ->
            sat?.let {
                SatItem(sat = it, navController, selectedSat)
            } ?: Text(text = "No information available for specific school")
        }
    }
}

/**
 * UI for the Sat item
 */
@Composable
fun SatItem(
    sat: Sat,
    navController: NavController? = null,
    selectedSat: ((Sat) -> Unit)? = null
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(10.dp)
    ) {
        Text(
            text = "SAT average score:",
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.Bold,
            fontStyle = FontStyle.Italic,
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(10.dp)
        )
        Text(text = "DBN School: ${sat?.dbn ?: "Invalid dbn"}")
        Spacer(modifier = Modifier.height(5.dp))
        Text(text = "School Name: ${sat?.schoolName ?: "Invalid school name"}")
        Spacer(modifier = Modifier.height(5.dp))
        Text(text = "Math Avg Score: ${sat?.satMathAvgScore ?: "Invalid math score"}")
        Spacer(modifier = Modifier.height(5.dp))
        Text(text = "Writing Avg Score: ${sat.satWritingAvgScore ?: "Invalid match score"}")
        Spacer(modifier = Modifier.height(5.dp))
        Text(text = "Reading Avg Score: ${sat.satCriticalReadingAvgScore ?: "Invalid reading score"}")
        Spacer(modifier = Modifier.height(5.dp))
    }
}