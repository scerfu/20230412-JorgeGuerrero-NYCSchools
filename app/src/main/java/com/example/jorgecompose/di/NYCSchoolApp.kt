package com.example.jorgecompose.di

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * App for use of dependency injection
 * Starting koin application
 */
class NYCSchoolApp: Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin{
            androidContext(this@NYCSchoolApp)
            modules(listOf(networkModule, repositoryModule, viewModelModule))
        }
    }
}