package com.example.jorgecompose.di

import com.example.jorgecompose.service.NYCSchoolRepository
import com.example.jorgecompose.service.NYCSchoolsAPI
import com.example.jorgecompose.service.NYCSchoolsRepositoryImpl
import com.example.jorgecompose.utils.BASE_URL
import com.example.jorgecompose.viewmodel.NYCSchoolViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Declaring modules to be use for Koin
 */
val networkModule = module {

    fun providesHttpLoggingInterceptor(): HttpLoggingInterceptor =
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

    fun providesOkHttpClient(
        httpLoggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor)
            .build()

    fun providesSchoolsApi(okHttpClient: OkHttpClient): NYCSchoolsAPI =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
            .create(NYCSchoolsAPI::class.java)

    single { providesHttpLoggingInterceptor() }
    single { providesOkHttpClient(get()) }
    single { providesSchoolsApi(get()) }
}

val repositoryModule = module {

    fun providesRepository(
        schoolsApi: NYCSchoolsAPI
    ): NYCSchoolRepository =
        NYCSchoolsRepositoryImpl(schoolsApi)

    single { providesRepository(get()) }
}

val viewModelModule = module {
    viewModel { NYCSchoolViewModel(get()) }
}