package com.example.jorgecompose

import com.example.jorgecompose.model.School
import com.example.jorgecompose.service.NYCSchoolRepository
import com.example.jorgecompose.service.NYCSchoolsAPI
import com.example.jorgecompose.service.NYCSchoolsRepositoryImpl
import com.example.jorgecompose.utils.UIState
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class RepositoryImplTest {

    private lateinit var repository: NYCSchoolRepository

    private val mockAPI: NYCSchoolsAPI = mockk(relaxed = true)

    private val testDispatcher = UnconfinedTestDispatcher()
    private val testScope = TestScope(testDispatcher)

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        repository = NYCSchoolsRepositoryImpl(mockAPI)
    }

    @After
    fun logOff() {
        Dispatchers.resetMain()
        clearAllMocks()
    }

    /**
     *  Test to verify the school data retrieved
     *  in a success state response
     */
    @Test
    fun getSchoolsSuccess() {

        coEvery { mockAPI.getNYCSchools() } returns mockk {
            every { isSuccessful } returns true
            every { body() } returns listOf(mockk(), mockk(), mockk(), mockk())
        }

        val states = mutableListOf<UIState<List<School>>>()
        val job = testScope.launch {
            repository.getNYCSchools().collect() {
                states.add(it)
                assertEquals(4, states.size)
            }
        }

        job.cancel()
    }

    /**
     *  Test to verify the school data retrieved
     *  in an error state response
     */
    @Test
    fun getSchoolsError() {

        coEvery { mockAPI.getNYCSchools() } returns mockk {
            every { isSuccessful } returns true
            every { body() } returns null
        }

        val job = testScope.launch {
            repository.getNYCSchools().collect() {
                if (it is UIState.ERROR) {

                    assert(it is UIState.ERROR)
                    assertEquals("Info not available", it.error)
                }
            }
        }
        job.cancel()
    }

    /**
     *  Test to verify the Sat data retrieved
     *  in a success state response
     */
    @Test
    fun getSat() {

        coEvery { mockAPI.getSatScore() } returns mockk {
            every { isSuccessful } returns true
            every { body() } returns mockk()
        }

        val job = testScope.launch {
            repository.getSatScore("17B543").collect() {
                if (it is UIState.SUCCESS) {

                    assert(it is UIState.SUCCESS)
                    assertEquals(1, it.response)
                }
            }
        }
        job.cancel()
    }

    /**
     *  Test to verify the Sat data retrieved
     *  in an error state response
     */
    @Test
    fun getSatError() {

        coEvery { mockAPI.getSatScore() } throws Exception("ERROR")

        val job = testScope.launch {
            repository.getSatScore("45TB23").collect() {
                if (it is UIState.ERROR) {

                    assert(it is UIState.SUCCESS)
                    assertEquals("ERROR", it.error)
                }
            }
        }
        job.cancel()
    }
}